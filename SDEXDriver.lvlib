﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the instrument driver for the SDEX (serial data exchange) controller. SDEX is a versatile interface implemented using a serial bus and has been developed by Harald Hahn, GSI (EE/KS). The VI Tree displays all the user-callable VIs of the instrument driver in an organized table. Please note, that the module address must be given in HEX format.

This driver should now also work with the watchdog functionality.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

Copyright (C) 
Gesellschaft f. Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: http://www-wnt.gsi.de/labview, d.beck@gsi.de

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details (http://www.gnu.org).</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!-*!!!*Q(C=V:;`&lt;B."%)@H9AK1I(#,+-#JU$7";3G)Z);3QF&gt;441V6J&amp;38J!E3I#16U`!!?10E*GH2P))&lt;(M"0A(2]N\?__!S+'Z"A,X-_`X[TM^`_]3ECK&gt;U6O30.NC;XNJ8@@H&lt;:[&gt;[KT:L@[FF=#=WQ@R^ZS':9P[NQUW`$XV\`&gt;L[OF&gt;6KUF7Z-&lt;`M676K6:_4`)&amp;8XK25H6`^XKSKJOSYMD!U7T^T\W;ZSB`:\@WSX#UR7WP.L:L?`VK7X1$L\PLY?5K^XX80`(2MOIQ&lt;PVK&lt;@XJ/27\KLV;Q8Z]OIRG[`@KO-JKBO\(_:!T&gt;Q@ZV]R_Y'`N,SN$&gt;^(^N`\_``?&gt;&gt;&lt;UX[SUWE3+2%%%Y9I&gt;XJ#`2!$`2!$`2!&gt;X2(&gt;X2(&gt;X2(.X2$.X2$.X2$6X2&amp;6X2&amp;6X4NXB3.I!O[I)OG6U-Q?$"15$1I%#3$)M%NY!FY!J[!B[]3]!1]!5`!%`#1)A&amp;0Q"0Q"$Q"$^UEY!FY!J[!*_#B6#+*&gt;+($%`"18BQ?B]@B=8A=(I95B]="=!:T#DN&amp;Q"$(&gt;"Y=(I@(Y?&amp;2("[(R_&amp;R?"Q?&lt;(&amp;Y("[(R_&amp;R?/C36M5448OBQU-:-8A-(I0(Y$&amp;Y+#U'D]&amp;D]"A]"A`$C=&amp;D]"A1RI"'=2$%['1E'&amp;]-(I/($T&amp;Y$"[$R_!R?,$3$FF;G:;GP&gt;$B58A5(I6(Y6&amp;Y+#%+D]+D]#A]#A^F2?&amp;2?"1?B5@B93B2?"1?B5="51:F?&amp;'++2W6*%61?0B,JU84,HECU84FHW:X5#5@1-E(3`+"E8Q1*'_QZ)W4P#'3&amp;VLS!EJ?'-E4FDQ2S9#3"Z:=5(+C,,EPC$FR3:Q3"]3-G")49NRW`=/*S_63&amp;IO&amp;T/&gt;TO&lt;S]F.045TEY/*$:&lt;#&lt;4[61GEYG-R_0_&lt;@79KWP&amp;[LV5]XTUZ@$*]?P^`;0RW[0[`/62?E;L2T_+_M8I_O2+XHW3U@PTKU@X0O_^?8LRM@;T\TOPTA[,:R=0Z@L$];AYG&gt;QP[G^\2@X]KKA@3.(7P3W[^^+`]'[5H&gt;8`2+MV_AEXC.T#!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.17.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="typedefs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SDEX.VISA Control.ctl" Type="VI" URL="../SDEX.VISA Control.ctl"/>
		<Item Name="SDEX.revisionType.ctl" Type="VI" URL="../SDEX.revisionType.ctl"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="SDEX.application example.vi" Type="VI" URL="../SDEX.application example.vi"/>
		<Item Name="SDEX.Close.vi" Type="VI" URL="../SDEX.Close.vi"/>
		<Item Name="SDEX.Error Message.vi" Type="VI" URL="../SDEX.Error Message.vi"/>
		<Item Name="SDEX.get library version.vi" Type="VI" URL="../SDEX.get library version.vi"/>
		<Item Name="SDEX.Initialize.vi" Type="VI" URL="../SDEX.Initialize.vi"/>
		<Item Name="SDEX.Read_0.vi" Type="VI" URL="../SDEX.Read_0.vi"/>
		<Item Name="SDEX.Read_E.vi" Type="VI" URL="../SDEX.Read_E.vi"/>
		<Item Name="SDEX.Read_I.vi" Type="VI" URL="../SDEX.Read_I.vi"/>
		<Item Name="SDEX.Revision Query.vi" Type="VI" URL="../SDEX.Revision Query.vi"/>
		<Item Name="SDEX.Write_0.vi" Type="VI" URL="../SDEX.Write_0.vi"/>
		<Item Name="SDEX.Write_1.vi" Type="VI" URL="../SDEX.Write_1.vi"/>
		<Item Name="SDEX.Write_2.vi" Type="VI" URL="../SDEX.Write_2.vi"/>
		<Item Name="SDEX.Write_C.vi" Type="VI" URL="../SDEX.Write_C.vi"/>
		<Item Name="SDEX.Write_I.vi" Type="VI" URL="../SDEX.Write_I.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="SDEX.calc check sum.vi" Type="VI" URL="../SDEX.calc check sum.vi"/>
		<Item Name="SDEX.call chain 2 string.vi" Type="VI" URL="../SDEX.call chain 2 string.vi"/>
		<Item Name="SDEX.Clean Up Initialize.vi" Type="VI" URL="../SDEX.Clean Up Initialize.vi"/>
		<Item Name="SDEX.compose address string.vi" Type="VI" URL="../SDEX.compose address string.vi"/>
		<Item Name="SDEX.connection counter.vi" Type="VI" URL="../SDEX.connection counter.vi"/>
		<Item Name="SDEX.parse read buffer.vi" Type="VI" URL="../SDEX.parse read buffer.vi"/>
		<Item Name="SDEX.test.vi" Type="VI" URL="../SDEX.test.vi"/>
		<Item Name="SDEX.Transaction.vi" Type="VI" URL="../SDEX.Transaction.vi"/>
	</Item>
	<Item Name="SDEX.VI Tree.vi" Type="VI" URL="../SDEX.VI Tree.vi"/>
	<Item Name="revisionsAndCommands.xls" Type="Document" URL="../revisionsAndCommands.xls"/>
</Library>
